from random import random, choices
from matplotlib import pyplot as plt

class Graph:
    def __init__(self, filepath):
        with open(filepath, "r") as graph_file:
            lines = graph_file.readlines()

        self.v_num, self.e_num, _ = list(map(int, lines[0].split()))

        self.adj = {}

        curr_idx = 1
        for line in lines[1:]:
            self.adj[curr_idx] = [int(x) for x in line.split()]
            curr_idx += 1

        self.all_nodes = list(self.adj.keys())

        self.all_edges = set()
        self.node_edges = dict()

        for node in self.all_nodes:
            self.node_edges[node] = set()

        for node in self.all_nodes:
            for neighbor in self.adj[node]:
                edge = self.get_edge(node, neighbor)
                self.all_edges.add(edge)
                self.node_edges[node].add(edge)
                self.node_edges[neighbor].add(edge)

    def get_edge(self, u, v):
        return (v, u) if v < u else (u, v)

    def get_neighbours(self, node):
        return self.adj[node]

    def get_node_edges(self, node):
        return self.node_edges[node]

    def get_covered_edges(self, vertex_cover): 
        covered_edges = set()
        
        for node in vertex_cover:
            covered_edges.update(self.get_node_edges(node))

        return covered_edges

    def count_covered_edges(self, vertex_cover):
        return len(self.get_covered_edges(vertex_cover))

    def is_vertex_cover(self, vertex_cover):
        return self.count_covered_edges(vertex_cover) == self.e_num

    def print_graph(self):
        print(f"Num of nodes: {len(self.all_nodes)}")
        print(f"Edges: ")
        list(map(print, self.all_edges))
        for node in self.all_nodes:
            print(f"Node {node} edges: ")
            list(map(print, self.get_node_edges(node)))

    def create_plot_coordinates(self):
        self.xs = list(range(1, self.v_num + 1))
        self.ys = choices(range(1, self.v_num + 1), k=self.v_num)
            
    def get_coordinates_for_plot(self):
        return self.xs, self.ys


    def print_solution(self, solution):
        xs, ys = self.get_coordinates_for_plot()
    
        plt.scatter(xs, ys, zorder=1) 
    
        for edge in self.all_edges:
            dot_xs = [xs[edge[0] - 1], xs[edge[1] - 1]]
            dot_ys = [ys[edge[0] - 1], ys[edge[1] - 1]]
            plt.plot(dot_xs, dot_ys, color='y', alpha=0.5, zorder=0)
    
        for node in solution:
            try:
                plt.scatter(xs[node - 1], ys[node - 1], color='r', zorder=1)
            except IndexError:
                print(f"INDEX ERROR: {node}, xs length: {len(xs)}")
    
        plt.show()
