from copy import deepcopy
from random import random

from graph import Graph

class Hybrid:
    def __init__(self, graph, plot_process=False, iter_plot_rate=0.3):
        self.graph = graph
        self.plot_process = plot_process
        self.iter_plot_rate = iter_plot_rate


    def calculate_degrees(self, nodes, visited_edges):
        degrees = [0 for _ in range(self.graph.v_num + 1)]
        for node in nodes:
            for edge in self.graph.node_edges[node]:
                if edge not in visited_edges:
                    degrees[node] += 1

        return degrees

    def find_max_degree_node(self, degrees):
        max_idx = 0
        max_degree = 0
        for i in range(1, len(degrees)):
            if degrees[i] >= max_degree:
                max_degree = degrees[i]
                max_idx = i

        return max_idx

    def get_solution(self, start=1):
        solution = []
        visited_edges = []
        nodes = list(range(1, self.graph.v_num + 1))
        
        while len(visited_edges) != self.graph.e_num:
            degrees = self.calculate_degrees(nodes, visited_edges)
            max_degree_node = self.find_max_degree_node(degrees)
            
            if max_degree_node == 0:
                break

            solution.append(max_degree_node)
            nodes.remove(max_degree_node)
            for edge in self.graph.node_edges[max_degree_node]:
                if edge not in visited_edges:
                    visited_edges.append(edge)

            if self.plot_process and random() < self.iter_plot_rate:
                self.graph.print_solution(solution)
                

        return len(solution), solution
