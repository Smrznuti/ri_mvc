from random import randint, sample, choice, random, choices
from copy import deepcopy

from graph import Graph
from GA.individual import Individual


class GA:
    def __init__(self, graph, iter_num=100, population_size=100, elitism_size=0, mutation_rate=0.8, plot_process=False):
        self.graph = graph
        self.iter_num = iter_num
        self.population_size = population_size
        self.elitism_size = elitism_size
        self.mutation_rate = mutation_rate
        self.plot_process = plot_process

    def init_population(self):
        population = []
        
        for i in range(self.population_size):
            number_of_vertexes = randint(1, self.graph.v_num)
            vertex_cover = sample(self.graph.all_nodes, number_of_vertexes)
            population.append(self.create_individual(vertex_cover))

        return population
    
    def get_total_fitness(self, population):
        total_fitness = 0
        for individual in population:
            total_fitness += individual.get_fitness()
        return total_fitness

    def get_crossover_probabilites(self, population, total_fitness):
        probabilites = []
        for individual in population:
            individual_weight = individual.get_fitness()/total_fitness
            probabilites.append(individual_weight)
        return probabilites

    def create_individual(self, vertex_cover):
        return Individual(self.graph, vertex_cover)

    def crossover(self, parent1, parent2):
        parent1_set = set(parent1)
        parent2_set = set(parent2)

        shared_nodes = parent1_set.intersection(parent2_set)

        child1 = self.create_individual(list(shared_nodes))
        child2 = self.create_individual(list(shared_nodes))

        self.remove_nodes(child1)
        return child1, child2

    def add_nodes(self, individual):
        individual_vertex_cover = individual.get_vertex_cover()
        nodes_not_included = list(set(range(1, self.graph.v_num)) - set(individual_vertex_cover))

        try:
            num_of_nodes_to_add = randint(1, len(nodes_not_included) - 1)
        except ValueError:
            return

        nodes_to_add = sample(nodes_not_included, k=num_of_nodes_to_add)

        for node in nodes_to_add:
            individual_vertex_cover.append(node)

        individual.set_vertex_cover(individual_vertex_cover)

    def remove_nodes(self, individual):
        individual_vertex_cover = individual.get_vertex_cover()

        try:
            num_of_nodes_to_remove = randint(1, len(individual_vertex_cover) - 1)
        except ValueError:
            return

        nodes_to_remove = sample(individual_vertex_cover, k=num_of_nodes_to_remove)

        for node in nodes_to_remove:
            individual_vertex_cover.remove(node)

        individual.set_vertex_cover(individual_vertex_cover)

    def mutate(self, individual):
        if random() < self.mutation_rate:
            self.add_nodes(individual)

    def make_next_gen(self, current_population, probabilites_to_crossover):
        next_generation = []

        while len(next_generation) + self.elitism_size < self.population_size:
            parents = choices(current_population, weights=probabilites_to_crossover, k=2)

            child1, child2 = self.crossover(parents[0].get_vertex_cover(), parents[1].get_vertex_cover())
            self.mutate(child1)
            self.mutate(child2)

            next_generation.append(child1)
            next_generation.append(child2)

        return next_generation
        
    def update_solution(self, population):
        self.solution = max(population, key=lambda individual: individual.get_fitness())
        

    def calculate_vertex_cover(self):
        self.solution = None

        population = self.init_population()
        
        for it in range(self.iter_num):
            population = sorted(population, key=lambda individual: (-1)*individual.get_fitness())

            new_population = population[:self.elitism_size]

            probabilites_to_crossover = self.get_crossover_probabilites(population, self.get_total_fitness(population))
            
            next_generation = self.make_next_gen(population, probabilites_to_crossover)
            new_population.extend(next_generation)

            population = deepcopy(new_population)
            self.update_solution(population)

            if self.plot_process:
                if it % 20 == 0:
                    self.graph.print_solution(self.solution.get_vertex_cover())


    def get_solution(self):
        self.calculate_vertex_cover()
        return len(self.solution.get_vertex_cover()), self.solution.get_vertex_cover()
