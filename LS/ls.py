from random import random
from copy import deepcopy

class SA:
    def __init__(self, graph, iter_num=100, init_true_rate=0.25, change_node_rate=0.1, plot_process=False):
        self.init_true_rate = init_true_rate
        self.change_node_rate = change_node_rate
        self.graph = graph
        self.iter_num = iter_num
        self.v_num = self.graph.v_num
        self.e_num = self.graph.e_num
        self.edges = self.graph.all_edges
        self.plot_process = plot_process

    def get_vertex_cover_from_inclusion_list(self, solution):
        solution_list = []

        for i in range(1, len(solution)):
            if solution[i]:
                solution_list.append(i)

        return solution_list

    def is_feasable(self, solution):
        solution_list = self.get_vertex_cover_from_inclusion_list(solution)
        
        if self.graph.is_vertex_cover(solution_list):
            return True

        return False

    def get_uncovered_edges(self, solution):
        solution_list = self.get_vertex_cover_from_inclusion_list(solution)

        uncovered_edges = self.edges - self.graph.get_covered_edges(solution_list)
        return uncovered_edges

    def make_feasable(self, new_solution):
        #new_solution = deepcopy(solution)
        if self.is_feasable(new_solution):
            return 

        uncovered_edges = self.get_uncovered_edges(new_solution)
        
        while not self.is_feasable(new_solution):
            edge = uncovered_edges.pop()
            new_solution[edge[0]] = True
            uncovered_edges = self.get_uncovered_edges(new_solution)

        #return new_solution

    def initialize(self):
        solution = [ random() < self.init_true_rate for _ in range(self.v_num + 1)]
        solution[0] = False
        if not self.is_feasable(solution):
            self.make_feasable(solution)

        return solution

    def calc_solution_value(self, solution_list):
        solution = self.get_vertex_cover_from_inclusion_list(solution_list)
        return self.v_num - len(solution)

    def make_small_change(self, solution):
        new_solution = deepcopy(solution)

        for i in range(1, len(new_solution)):
            if random() < self.change_node_rate:
                new_solution[i] = False

        self.make_feasable(new_solution)
        #new_solution = self.make_feasable(new_solution)
        return new_solution

    def simulated_annealing(self):
        solution = self.initialize()
        value = self.calc_solution_value(solution)
        best_solution = deepcopy(solution)
        best_value = value
        
        for it in range(1, self.iter_num):
            if self.plot_process:
                if self.iter_num % it == 5:
                    self.graph.print_solution(self.get_vertex_cover_from_inclusion_list(best_solution))

            new_solution = self.make_small_change(solution)
            new_value = self.calc_solution_value(new_solution)
            
            if new_value < value:
                value = new_value
                solution = deepcopy(new_solution)
                if new_value < best_value:
                    best_value = new_value
                    best_solution = deepcopy(new_solution)
            else:
                p = 1 / it ** 0.5
                q = random()
                if q < p:
                    value = new_value
                    solution = deepcopy(new_solution)

                    
        return best_solution, best_value

    def get_solution(self):
        solution, value = self.simulated_annealing()
        return sum(solution), self.get_vertex_cover_from_inclusion_list(solution)
