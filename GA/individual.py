class Individual:
    def __init__(self, graph, vertex_cover):
        self.vertex_cover = vertex_cover
        self.graph = graph
        self.calculate_fitness()

    def calculate_fitness(self):
        if len(self.vertex_cover) == 0:
            self.fitness = 0
            return

        covered_edges = self.graph.count_covered_edges(self.vertex_cover)
        if not self.graph.is_vertex_cover(self.vertex_cover):
            self.fitness = 1e-10
        else:
            self.fitness = self.graph.v_num - len(self.vertex_cover)

    def get_vertex_cover(self):
        return self.vertex_cover

    def get_fitness(self):
        return self.fitness

    def set_vertex_cover(self, vertex_cover):
        self.vertex_cover = vertex_cover
        self.calculate_fitness()

    def __str__(self):
        return f"{len(self.get_vertex_cover())}, with fitness: {self.get_fitness()}"
