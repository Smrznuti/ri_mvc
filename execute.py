import argparse
import time
import random
from matplotlib import pyplot as plt

from GA.ga import GA
from LS.ls import SA
from hybrid.hybrid import Hybrid
from graph import Graph

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("--graph", required=True, help="Graph path")
    parser.add_argument("-p", action='store_true', help="Plot solutions")
    parser.add_argument("-d", action='store_true', help="Hybrid")
    parser.add_argument("-g", action='store_true', help="Genetic")
    parser.add_argument("-s", action='store_true', help="Simualated annealing")
    parser.add_argument("-a", action='store_true', help="All algortihms")

    args = parser.parse_args()
    return args

def get_hybrid_solution(args, graph):
    start_time = time.time()
    hybrid = Hybrid(graph, plot_process=(args.p and args.d))
    length, solution = hybrid.get_solution()
    hybrid_time = time.time() - start_time

    print(f"Solution length for Hybrid Greedy algorithm: {length}")
    print(f"Time for Hybrid Greedy algorithm: {hybrid_time}", end=f"\n{80*'-'}\n\n\n")

    with open("results.txt", "a") as result:
        result.write(f"Hybrid: v number: {length}, time = {hybrid_time}\n") 

    if args.p:
        graph.print_solution(solution)

def get_ga_solution(args, graph):
    start_time = time.time()
    mvc = GA(graph, mutation_rate=0.05, iter_num=100, population_size=500, elitism_size=0, plot_process=(args.p and args.g))
    ga_length, ga_solution = mvc.get_solution()
    ga_time = time.time() - start_time

    print(f"Solution length for Genetic algorithm: {ga_length}")
    print(f"Time for Genetic algorithm: {ga_time}", end=f"\n{80*'-'}\n\n\n")

    with open("results.txt", "a") as result:
        result.write(f"Genetic: v number: {ga_length}, time = {ga_time}\n")

    if args.p:
        graph.print_solution(ga_solution)

def get_sa_solution(args, graph):
    start_time = time.time()
    sa = SA(graph, iter_num=100, init_true_rate=0.1, change_node_rate=0.5, plot_process=(args.p and args.s))
    sa_length, sa_solution = sa.get_solution()
    sa_time = time.time() - start_time

    print(f"Solution length for Simualated annealing algotrithm: {sa_length}")
    print(f"Time for Simualated annealing algorithm: {sa_time}", end=f"\n{80*'-'}\n\n\n")

    with open("results.txt", "a") as result:
        result.write(f"Simualated annealing: v number: {sa_length}, time = {sa_time}\n")

    if args.p:
        graph.print_solution(sa_solution)

def main(args):
    graph = Graph(args.graph)

    if args.p:
        graph.create_plot_coordinates()

    print(f"\n\n\n\nGraph in use: {args.graph}", end=f"\n{80*'-'}\n\n\n")
    with open("results.txt", "a") as result:
        result.write(f"Graph in use: {args.graph}\n")

    if args.d or args.a:
        get_hybrid_solution(args, graph)

    if args.g or args.a:
        get_ga_solution(args, graph)

    if args.s or args.a:
        get_sa_solution(args, graph)



if __name__ == "__main__":
    args = parse_arguments()
    main(args)
